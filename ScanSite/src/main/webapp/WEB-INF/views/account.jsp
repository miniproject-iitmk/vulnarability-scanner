<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>accounts</title>
</head>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<div class="row">
					<div class="small-12 large-12 columns">
						<h4>Account</h4>
					</div>
				</div>
				<div class="row">

					<div class="small-6 large-6 columns">
						<div class="callout end">
							<h6>Change Password</h6>
							<form>
								<div class="row">
									<div class="small-4 columns">
										<label for="middle-label" class="text-right middle">Current
											Password</label>
									</div>
									<div class="small-8 columns">
										<input type="password" id="middle-label"
											placeholder="Current Password" name="Current Password">
									</div>
								</div>
								<div class="row">
									<div class="small-4 columns">
										<label for="middle-label" class="text-right middle">New
											Password</label>
									</div>
									<div class="small-8 columns">
										<input type="password" id="middle-label"
											placeholder="New Password" name="New Password">
									</div>
								</div>
								<div class="row">
									<div class="small-4 columns">
										<label for="middle-label" class="text-right middle">Re-Enter
											Password</label>
									</div>
									<div class="small-8 columns">
										<input type="password" id="middle-label"
											placeholder="Re-Enter Password" name="Re-Enter Password">
									</div>
								</div>
								<div class="row">
									<div class="small-4 columns"></div>
									<div class="small-8 columns">
										<button type="submit" class="small button">Change</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="small-6 large-6 columns end">
						<div class="callout">
							<h6>Delete Account</h6>
							<div class="row">
								<div class="small-12 large-12 columns end">
									<button type="submit"
										class="alert button expanded button float-center">Delete
										Account</button>
								</div>
							</div>
						</div>
					</div>


				</div>

				<!-- sample html ends -->



				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="static/js/vendor/jquery.min.js"></script>
	<script src="static/js/vendor/what-input.min.js"></script>
	<script src="static/js/foundation.min.js"></script>
	<script src="static/js/app.js"></script>
</body>
</html>