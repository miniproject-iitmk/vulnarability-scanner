<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
</head>
<body>
	<div class="row">
		<div class="medium-6 medium-centered large-3 large-centered columns"
			id="login">
			<c:url var="loginUrl" value="/login" />
			<form action="${loginUrl}" method="post">
				<div class="column log-in-form">
					<h4 class="text-center">Log in to Scan Site</h4>
					<label>Username <input type="text" placeholder="Username"
						name="username">
					</label> <label>Password <input type="password"
						placeholder="Password" name="password">
					</label> <input id="remember-me" type="checkbox"><label
						for="remember-me">Remember me</label> <input type="hidden"
						name="${_csrf.parameterName}" value="${_csrf.token}" />
					<p>
						<input type="submit" class="button expanded" value="Log In" />
					</p>
					<c:if test="${param.error != null}">
						<span class="alert float-center label">Incorrect Username
							or Password!</span>
					</c:if>
					<c:if test="${param.logout != null}">
						<span class="success float-center label">You have been logged out successfully!</span>
					</c:if>
				</div>
			</form>
		</div>
	</div>
</body>
</html>