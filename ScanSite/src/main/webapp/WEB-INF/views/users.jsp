<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Users</title>
<link rel="stylesheet" href="static/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="static/css/app.css" />
<link href="static/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='static/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
<script type="text/javascript">
	var focuzzedScanId = null;
</script>
</head>
<body>
	<c:url var="addUser" value="/addUser" />
	<c:url var="deleteUser" value="/deleteUser" />
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<div class="row">
					<div class="small-12 large-12 columns">
						<h4>Users</h4>
					</div>
				</div>
				<div class="row">
					<div class="small-12 large-12 columns">
						<div class="row">
							<div class="small-12 large-12 columns">
								<div class="callout">
									<h6>Manage Users</h6>
									<table id="userTable" class="hover">
										<thead>
											<tr>
												<th width="120">User Name</th>
												<th width="150">Role</th>
												<th width="120">Action</th>
											</tr>
										</thead>
										<tbody >
											<c:forEach var="user" items="${userList}">
												<tr>
													<td>${user.userName}</td>
													<c:forEach var="role" items="${user.userRoles }">
														<c:choose>
															<c:when test="${ role.isDefault == 'TRUE' }">
																<c:set var="enable" value="1"></c:set>
															</c:when>
															<c:otherwise>
																<c:set var="enable" value="0"></c:set>
															</c:otherwise>
														</c:choose>
														<c:choose>
															<c:when test="${enable ==1 }">
																<td><label>Admin</label></td>
																<td><button disabled="disabled" type="button"
																		class="success button small button ">Reset
																		Password</button>
																	<button disabled="disabled" type="button"
																		class="alert button small button ">Delete</button></td>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${role.role.type == 'ADMIN' }">
																		<c:set var="roleAdmin" value="1"></c:set>
																	</c:when>
																	<c:otherwise>
																		<c:set var="roleAdmin" value="0"></c:set>
																	</c:otherwise>
																</c:choose>
																<td><c:choose>
																		<c:when test="${roleAdmin == 1 }">
																			<label>Admin</label>
																		</c:when>
																		<c:otherwise>
																			<label>User</label>
																		</c:otherwise>
																	</c:choose></td>
																<td id="restRow"><form id="testForm" class="formEdit" action="" method="post">
																		<button id="restButton" type="button" data-open="resetPassword"
																			class="success button small button ">Reset
																			Password</button>
																	</form>
																	<form class="formEdit" action="${deleteUser}" method="post">
																		<input type="hidden" name="user_name"
																			value="${user.userName}"> <input
																			type="submit" class="alert button small button "
																			value="Delete" /><input type="hidden"
																			name="${_csrf.parameterName}" value="${_csrf.token}" />
																	</form></td>
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</tr>
											</c:forEach>

											<form action="${addUser}" method="post">
												<tr>
													<td><input type="text" placeholder="user name"
														name="user_name"></td>
													<td><select name="selection">
															<option value="ADMIN">Admin</option>
															<option value="USER">User</option>
													</select></td>
													<td><div class="row">
															<div class="columns large-8">
																<input type="password"
																	aria-describedby="passwordHelpText"
																	placeholder="password" name="password" /> <input
																	type="hidden" name="${_csrf.parameterName}"
																	value="${_csrf.token}" />
															</div>
															<div class="columns large-4 end">
																<input type="submit"
																	class="success button small button " value="Add">
															</div>
														</div></td>
												</tr>
											</form>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- sample html ends -->
				<div class="tiny reveal" id="resetPassword" data-reveal>

					<div class="row">
						<div class="small-12 columns closeButton">
							<form action="#" method="post">
								<div class="small-6 columns">
									<label for="right-label" class="text-right">New
										Password</label>
								</div>
								<div class="small-6 columns">
									<input type="password" id="right-label" name="newPassword">
								</div>
								<div class="row">
									<div class="small-6 columns"></div>
									<div class="small-6 columns ">
										<button type="Submit" class="alert button">Reset</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<button class="close-button" data-close=""
						aria-label="Close reveal" type="button">
						<span aria-hidden="true">�</span>
					</button>

				</div>
				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script src="static/js/vendor/jquery.min.js"></script>
	<script src="static/js/vendor/what-input.min.js"></script>
	<script src="static/js/foundation.min.js"></script>
	<script src="static/js/app.js"></script>
</body>
</html>