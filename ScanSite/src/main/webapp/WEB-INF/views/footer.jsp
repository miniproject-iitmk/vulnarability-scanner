<footer class="footer">
	<div class="row">
		<div class="small-12 medium-6 medium-push-6 columns">
			<p class="logo show-for-small-only">
				<i class="fi-target"></i> Scanner
			</p>
		</div>
		<div class="small-12 medium-6 medium-pull-6 columns">
			<p class="logo hide-for-small-only">
				<i class="fi-target"></i> Scanner
			</p>
			<p class="footer-links">
				<a href="#">Blog</a> <a href="#">About</a> <a href="#">Contact</a>
			</p>
			<ul class="inline-list social">
				<a href="#"><i class="fi-social-facebook"></i></a>
				<a href="#"><i class="fi-social-twitter"></i></a>
				<a href="#"><i class="fi-social-linkedin"></i></a>
				<a href="#"><i class="fi-social-github"></i></a>
			</ul>
			<p class="copywrite">Copyleft � 2016</p>
		</div>
	</div>
</footer>