<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Site Scan - Home</title>
<link rel="stylesheet"
	href="<c:url value = "/static/css/foundation.css" />" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="<c:url value = "/static/css/app.css" />" />
<link href="<c:url value = "/static/css/font-awesome.min.css" />"
	rel="stylesheet" type='text/css'>
<link href='<c:url value = "/static/css/foundation-icons.css" />'
	rel='stylesheet' type='text/css'>
<c:url var="detailedReportUrl" value="/detailedReport/"></c:url>
<script type="text/javascript">
	var focuzzedScanId = "${scanId}";
</script>
</head>
<body>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large"
				id="offCanvasLeft" data-off-canvas>
				<div class="row">
					<!-- include the html written in sideBar.jsp -->
					<jsp:include page="sideBar.jsp" />
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<!-- include the html written in header.jsp -->
				<jsp:include page="header.jsp"></jsp:include>



				<!-- add the html for new pages here-->
				<!-- delete the sample html and add your own -->
				<!-- sample html starts -->
				<div class="row">
					<div class="large-10 columns">
						<h4>Reports</h4>

						<c:if test="${listOfRecords != null}">
							<c:forEach var="report" items="${listOfRecords}">
								<c:if test="${report.severity == 'Low'}">
									<div class="callout alertlow">
										<a href="${detailedReportUrl}${report.mappingId}"><b>${report.test.name}</b></a><br />
										Found at: ${report.url.url}
									</div>
								</c:if>
								<c:if test="${report.severity == 'Medium'}">
									<div class="callout alertmedium">
										<a href="${detailedReportUrl}${report.mappingId}"><b>${report.test.name}</b></a><br />
										Found at: ${report.url.url}
									</div>
								</c:if>
								<c:if test="${report.severity == 'High'}">
									<div class="callout alerthigh">
										<a href="${detailedReportUrl}${report.mappingId}"><b>${report.test.name}</b></a><br />
										Found at: ${report.url.url}
									</div>
								</c:if>
							</c:forEach>
						</c:if>

					</div>
				</div>
				<!-- sample html ends -->



				<!-- include the html written in footer.jsp-->
				<jsp:include page="footer.jsp"></jsp:include>

			</div>
		</div>
	</div>
	<script src="<c:url value = "/static/js/vendor/jquery.min.js" />"></script>
	<script src="<c:url value = "/static/js/vendor/what-input.min.js" />"></script>
	<script src="<c:url value = "/static/js/foundation.min.js" />"></script>
	<script src="<c:url value = "/static/js/app.js" />"></script>
</body>
</html>