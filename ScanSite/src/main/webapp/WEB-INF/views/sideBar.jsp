<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<div class="medium-12 medium-centered columns left-menu-container">
	<c:url var="scanSettingsUrl" value="/scanSettings/" />
	<c:url var="activeScanUrl" value="/activeScan" />
	<c:url var="previousReportsUrl" value="/previousReports/" />
	<ul class="vertical accordion-menu menu" data-accordion-menu
		id="leftAccordianMenu">
		<li class="accordion-item-withou-child righ-menu-group-header"><a
			href="#">Active Scans</a></li>

		<c:choose>
			<c:when test="${not empty activeScan}">
				<li><a href="${activeScanUrl}">${activeScan.primaryUrl}</a>
					<ul class="menu vertical sublevel-1">
						<li><a class="subitem" href="${previousReportsUrl}${activeScan.scanId}">Previous Reports</a></li>
					</ul>
				</li>
			</c:when>
			<c:otherwise>
				<li><a href="#">No scans running!</a></li>
			</c:otherwise>
		</c:choose>


		<li class="accordion-item-withou-child"><button
				class="small button float-center new-scan-button" href="#">New
				Scan</button></li>
		<li class="accordion-item-withou-child righ-menu-group-header"><a
			href="#">Previous Scans</a></li>
		<c:choose>
			<c:when test="${not empty inactiveScans}">
				<c:forEach var="scan" items="${inactiveScans}">

					<li><a href="${scanSettingsUrl}${scan.scanId}">${scan.primaryUrl}</a>
						<ul class="menu vertical sublevel-1" title="${scan.scanId}">
							<li><a class="subitem" href="${previousReportsUrl}${scan.scanId}">Previous Reports</a></li>
						</ul></li>

				</c:forEach>
			</c:when>
			<c:otherwise>
				<li><a href="#">It is lonely here!</a></li>
			</c:otherwise>
		</c:choose>

		<li class="accordion-item-withou-child righ-menu-group-header"><a
			href="#">Settings</a></li>
		<sec:authorize access="hasRole('ADMIN')">
			<li class="accordion-item-withou-child"><a href="#">Plugins</a></li>
			<li class="accordion-item-withou-child"><a
				href="<c:url value="/manageUser" />">Users</a></li>
		</sec:authorize>
		<li class="accordion-item-withou-child"><a
			href="<c:url value="/manageAccounts" />">Account</a></li>
	</ul>
</div>