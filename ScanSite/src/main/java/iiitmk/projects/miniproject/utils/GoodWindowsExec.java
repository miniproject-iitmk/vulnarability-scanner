package iiitmk.projects.miniproject.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

class StreamGobbler extends Thread {
	InputStream is;
	OutputStream os;
	String type;
	Integer status;

	StreamGobbler(InputStream is, String type, OutputStream os, Integer status) {
		this.is = is;
		this.type = type;
		this.os = os;
		this.status = status;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			StringBuilder output = new StringBuilder();
			int singleChar;
			while ((singleChar = br.read()) != -1) {
				output.append(Character.toChars(singleChar));
				System.out.print(Character.toChars(singleChar));
				//System.out.println(output);
				if (output.indexOf("Do you want to continue? [Y/n]") != -1) {
					os.write("Yes\n".getBytes());
					os.flush();
					output = new StringBuilder();
					status = 1;
				}
				//Already installed
				if (output.indexOf("0 newly installed") != -1) {
					output = new StringBuilder();
					status = 2;
				}
				//Not found
				if (output.indexOf("Unable to locate package") != -1) {
					output = new StringBuilder();
					status = 3;
				}
				//connection problem
				if (output.indexOf("Unable to fetch some archives") != -1) {
					output = new StringBuilder();
					status = 4;
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}

public class GoodWindowsExec {
	public  void runMain() {
		try {
			Runtime rt = Runtime.getRuntime();
			Integer status = 0;
			Process proc = rt.exec("python /opt/sqlmap/sqlmap.py -v 2 --url http://google-gruyere.appspot.com/138191955499/login?uid=id& --batch --level=5 --risk=3 --technique=U --user-agent=Mozilla");
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR", proc.getOutputStream(), status);

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT", proc.getOutputStream(), status);
			
			// kick them off
			outputGobbler.start();
			errorGobbler.start();
			// any error???
			errorGobbler.join();
			outputGobbler.join();
			
			
			int exitVal= proc.waitFor();

			System.out.println("ExitValue: " + exitVal + " " );
			if(exitVal!=0){
				if(errorGobbler.status == 3){
					System.out.println("Package is not available!");
				}else if(errorGobbler.status == 4){
					System.out.println("Connection problems, check internet!");
				}
			}else{
				if(outputGobbler.status == 1){
					System.out.println("Installed successfully!");
				}else if(outputGobbler.status == 2){
					System.out.println("Already installed!");
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
}
