package iiitmk.projects.miniproject.utils;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.service.RecordService;
import iiitmk.projects.miniproject.service.ScanService;

public class ScanScheduler {
	
	private TaskScheduler scheduler;
	 
	ScanSettings scanSettings;
	
	ScanService scanService;
	
	RecordService recordService;
	
	/*Runnable scanRunner = new Runnable(){
	    @Override
	    public void run() {
	    	       	 
	    }
	};*/
	ScanRunner scanRunner = new ScanRunner();

	@Async
	public void executeTaskT() {
	    ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
	    scheduler = new ConcurrentTaskScheduler(localExecutor);
	    scanRunner.setScanSettings(this.scanSettings);
	    scanRunner.setScanService(scanService);
	    scanRunner.setRecordService(recordService);
	    scheduler.schedule(scanRunner, new Date());//today at 8 pm UTC - replace it with any timestamp in miliseconds to text
	}

	public ScanSettings getScanSettings() {
		return scanSettings;
	}

	public void setScanSettings(ScanSettings scanSettings) {
		this.scanSettings = scanSettings;
	}

	public ScanService getScanService() {
		return scanService;
	}

	public void setScanService(ScanService scanService) {
		this.scanService = scanService;
	}

	public RecordService getRecordService() {
		return recordService;
	}

	public void setRecordService(RecordService recordService) {
		this.recordService = recordService;
	}
	
	
}