package iiitmk.projects.miniproject.utils;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import iiitmk.projects.miniproject.beans.PluginTest;
import iiitmk.projects.miniproject.beans.ScanCookieCollection;
import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanRecordDetailed;
import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.beans.ScanUrlCollection;
import iiitmk.projects.miniproject.commons.Constants;
import iiitmk.projects.miniproject.service.RecordService;
import iiitmk.projects.miniproject.service.ScanService;

public class ScanRunner extends Thread {

	@Autowired
	ScanService scanService;
	ScanSettings scanSettings;
	@Autowired
	RecordService recordService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void run() {

		String scanType;
		String userAgent;
		String primaryUrl;
		String statusString = Constants.SCAN_STATUS_INITIAL;
		String severity;
		Integer percentCompleted = 0;
		Integer countLow = 0;
		Integer countMedium = 0;
		Integer countHigh = 0;
		List<ScanCookieCollection> cookies = null;
		List<PluginTest> testList;
		Crawler crawler = new Crawler();
		ScanRecordAbstract scanRecord;
		Collection<ScanRecordDetailed> detailedRecordList = new ArrayList<ScanRecordDetailed>();
			

		try {

			// get scan details and urls
			scanType = scanSettings.getScanType();
			primaryUrl = scanSettings.getPrimaryUrl();
			List<ScanUrlCollection> scanUrlList;

			if (scanSettings.getListOfCookies() != null) {
				cookies = (List) scanSettings.getListOfCookies();
				crawler.setCookies(cookies);
			}
			if (scanSettings.getUserAgent() != null) {
				userAgent = scanSettings.getUserAgent();
				crawler.setUserAgent(userAgent);
			}
			
			System.out.println("Scan type----------"+ scanType);
			//Insert one record to ScanRecordAbstract
			scanRecord = new ScanRecordAbstract();
			scanRecord.setCountHigh(0);
			scanRecord.setCountLow(0);
			scanRecord.setCountMedium(0);
			scanRecord.setScan(scanSettings);
			scanRecord.setScannedOn(new Date(new java.util.Date().getTime()));
			int recordPk = recordService.insertRecord(scanRecord);
			scanRecord.setScanRecordId(recordPk);
			
			if (scanType.equals(Constants.SCAN_TYPE_FULL)) {
				System.out.println("Scan type is full!");
				statusString += " : " + Constants.SCAN_STATUS_CRAWLING;
				scanService.updateScanStatus(scanSettings.getScanId(), statusString, percentCompleted, true, recordPk);
				scanUrlList = crawler.crawl(primaryUrl);
			} else {
				scanService.updateScanStatus(scanSettings.getScanId(), statusString, percentCompleted, true, recordPk);
				scanUrlList = (List) scanSettings.getListOfUrls();
			}

			// get tests
			testList = (List) scanSettings.getTests();

			// run tests
			percentCompleted = 1;
			
			for (PluginTest test : testList) {

				// check if require parameter
				Boolean requireParameterInUrl = test.getRequireParameterizedUrl();
				Boolean isSuccess;
				String executionPath = test.getPlugin().getExecutionPath();
				String arguments = test.getArguments();

				statusString = test.getPlugin().getName() + " - " + test.getName();
				scanService.updateScanStatus(scanSettings.getScanId(), statusString, percentCompleted, true, recordPk);

				System.out.println("Size of scanurlList:  " + scanUrlList.size());
				for (ScanUrlCollection url : scanUrlList) {
					
					if (requireParameterInUrl) {
						if (!url.getHasParameter()) {
							continue;
						}
					}

					// replace dynamic arguments
					if (arguments.contains(Constants.DYNAMIC_ARG_URL)) {
						arguments = arguments.replace(Constants.DYNAMIC_ARG_URL, url.getUrl());
					}
					if (arguments.contains(Constants.DYNAMIC_ARG_UA)) {

						if (scanSettings.getUserAgent() != null) {
							arguments = arguments.replace(Constants.DYNAMIC_ARG_UA, scanSettings.getUserAgent());
						} else {
							arguments = arguments.replace(Constants.DYNAMIC_ARG_UA, "Mozilla");
						}
					}
					if (arguments.contains(Constants.DYNAMIC_ARG_COOKIE)) {

						if (!cookies.isEmpty()) {
							String cookieString = "";

							for (ScanCookieCollection cookie : cookies) {
								cookieString += cookie.getCookieName() + "=" + cookie.getCookieVallue() + ";";
							}
							cookieString = cookieString.substring(0, cookieString.length() - 1);
							arguments = arguments.replace(Constants.DYNAMIC_ARG_COOKIE, cookieString);
						} else {
							arguments = arguments.replace("--cookie " + Constants.DYNAMIC_ARG_COOKIE, "");
						}
					}
					
					System.out.println("Argument------------------------------" + arguments);
					
					//execute process
					ProcessExecuter processExecuter = new ProcessExecuter();
					processExecuter.setCommand(executionPath + " " + arguments);
					processExecuter.setSuccessCondition(test.getSuccessCondition());
					isSuccess = processExecuter.executeProcess();
					
					if(!isSuccess){
						//create records if a test fails
						severity = test.getSeverity();
						if (severity.equalsIgnoreCase(Constants.SEVERITY_HIGH)) {
							countHigh++;
						}else if(severity.equalsIgnoreCase(Constants.SEVERITY_MEDIUM)){
							countMedium++;
						}else if(severity.equalsIgnoreCase(Constants.SEVERITY_LOW)){
							countLow++;
						}
						ScanRecordDetailed detailedRecord = new ScanRecordDetailed();
						detailedRecord.setScanRecord(scanRecord);
						detailedRecord.setSeverity(severity);
						detailedRecord.setTest(test);
						detailedRecord.setUrl(url);
/*						detailedRecordList.add(detailedRecord);
*/						
/*						scanRecord.setScanRecords(detailedRecordList);
*/						scanRecord.setCountHigh(countHigh);
						scanRecord.setCountMedium(countMedium);
						scanRecord.setCountLow(countLow);
						
						recordService.insertDetailedScanRecord(detailedRecord);
						
						recordService.updateRecord(scanRecord);						
					}
				}

				percentCompleted += 99 / testList.size();
			}
			//test completion
			scanService.updateScanStatus(scanSettings.getScanId(), "", 0, false, recordPk);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ScanSettings getScanSettings() {
		return scanSettings;
	}

	public void setScanSettings(ScanSettings scanSettings) {
		this.scanSettings = scanSettings;
	}

	public ScanService getScanService() {
		return scanService;
	}

	public void setScanService(ScanService scanService) {
		this.scanService = scanService;
	}

	public RecordService getRecordService() {
		return recordService;
	}

	public void setRecordService(RecordService recordService) {
		this.recordService = recordService;
	}
	
	
}
