package iiitmk.projects.miniproject.service;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanRecordDetailed;

public interface RecordService {

	int insertRecord(ScanRecordAbstract scanRecord);

	void updateRecord(ScanRecordAbstract scanRecord);
	
	ScanRecordAbstract getAbstractRecordById(Integer id);
	
	Integer insertDetailedScanRecord(ScanRecordDetailed scanRecord);
	ScanRecordDetailed getDetailedRecordById(Integer id);
}
