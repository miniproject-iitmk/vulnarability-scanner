package iiitmk.projects.miniproject.service;

import java.util.Collection;

import org.json.JSONArray;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanRecordDetailed;
import iiitmk.projects.miniproject.beans.ScanSettings;

public interface ScanService {
	ScanSettings getScanById(Integer scanId);
	//return the list of previous or inactive scans of a user
	Collection<ScanSettings> getInactiveScansByUsername(String userName);
	ScanSettings getActiveScansByUsername(String userName);
	void updateScanStatus(int scanId, String scanStatus, int percentageComplete, Boolean isActive, Integer recordPk);
	Integer insertNewScan(ScanSettings scanSettings);
	JSONArray getCrawledUrls(String primaryUrl);
	Collection<ScanRecordAbstract> getAbstractRecords(Integer scanId);
	


}
