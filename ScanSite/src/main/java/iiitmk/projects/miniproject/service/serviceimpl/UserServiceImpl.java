package iiitmk.projects.miniproject.service.serviceimpl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iiitmk.projects.miniproject.beans.Role;
import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.beans.User;
import iiitmk.projects.miniproject.dao.RoleDao;
import iiitmk.projects.miniproject.dao.UserDao;
import iiitmk.projects.miniproject.service.UserService;

/**
 * Various user service methods
 *
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;
	@Autowired
	private RoleDao roleDao;
	
	//find a user by user's id
	public User findById(int id) {
		return userDao.findById(id);
	}

	//find a user by his/her username  
	public User findByUsename(String sso) {
		return userDao.findBySSO(sso);
	}
	
	//to do
	public Role getUserRole(int roleId) {		
		return roleDao.findById(roleId);
	}

	//get the list of all users as an arrayList
	@Override
	public Collection<User> getAllUsers() {		
		return userDao.getAllUsers();
	}

	@Override
	public void addUser(String userName,String password,String roleName) {
		// TODO Auto-generated method stub
		
		User user = new User();
		Role role = roleDao.findByName(roleName);
		Set<Role> userRoles= new HashSet<Role>();
		userRoles.add(role);
		user.setUserName(userName);
		user.setPassword(password);
		user.setRoles(userRoles);
		userDao.addUser(user);
	}
	
}
