package iiitmk.projects.miniproject.service.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanRecordDetailed;
import iiitmk.projects.miniproject.dao.DetailedRecordDao;
import iiitmk.projects.miniproject.dao.RecordDao;
import iiitmk.projects.miniproject.service.RecordService;

@Service("recordService")
@Transactional
public class RecordServiceImpl implements RecordService {
	
	@Autowired
	RecordDao recordDao;
	@Autowired
	DetailedRecordDao detailedRecordDao;
	
	@Override
	public int insertRecord(ScanRecordAbstract scanRecord) {
		
		return recordDao.insertRecord(scanRecord);
	}

	@Override
	public void updateRecord(ScanRecordAbstract scanRecord) {
		recordDao.updateRecord(scanRecord);
		
	}

	@Override
	public ScanRecordAbstract getAbstractRecordById(Integer id) {
		// TODO Auto-generated method stub
		return recordDao.getAbstractRecordById(id);
	}
	
	@Override
	public Integer insertDetailedScanRecord(ScanRecordDetailed scanRecord) {
		// TODO Auto-generated method stub
		return detailedRecordDao.insertDetailedScanRecord(scanRecord);
	}
	
	@Override
	public ScanRecordDetailed getDetailedRecordById(Integer id) {
		// TODO Auto-generated method stub
		return detailedRecordDao.getDetailedRecordById(id);
	}

}
