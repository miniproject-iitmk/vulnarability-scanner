package iiitmk.projects.miniproject.service.serviceimpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iiitmk.projects.miniproject.beans.Plugin;
import iiitmk.projects.miniproject.beans.PluginTest;
import iiitmk.projects.miniproject.commons.Constants;
import iiitmk.projects.miniproject.dao.PluginDao;
import iiitmk.projects.miniproject.service.PluginService;
import iiitmk.projects.miniproject.utils.Md5Generator;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

@Service("pluginService")
@Transactional
public class PluginServiceImpl implements PluginService {

	@Autowired
	private Environment environment;

	@Autowired
	private Md5Generator md5Generator;

	@Autowired
	private PluginDao pluginDao;

	@Override
	public Boolean loadPlugins() {

		SAXReader reader = new SAXReader();
		try {
			File pluginBaseDirectory = new File(environment.getProperty("plugin.base_directory"));
			File[] pluginDirectories = pluginBaseDirectory.listFiles();

			for (File pluginDirectory : pluginDirectories) {
				if (pluginDirectory.isDirectory()) {
					Plugin plugin = new Plugin();

					File pluginDefinitionXmlFile = new File(
							pluginDirectory.getAbsolutePath() + "/" + Constants.PLUGIN_DEF_XML);
					Document pluginDefinitionXml = reader.read(pluginDefinitionXmlFile);

					Node nameNode = pluginDefinitionXml.selectSingleNode(
							"//" + Constants.PLUGIN_DEF_XML_ROOT_TAG + "/" + Constants.PLUGIN_DEF_XML_NAME_TAG);
					Node execPathNode = pluginDefinitionXml.selectSingleNode(
							"//" + Constants.PLUGIN_DEF_XML_ROOT_TAG + "/" + Constants.PLUGIN_DEF_XML_EXEC_PATH_TAG);
					Node sequenceNode = pluginDefinitionXml.selectSingleNode(
							"//" + Constants.PLUGIN_DEF_XML_ROOT_TAG + "/" + Constants.PLUGIN_DEF_XML_SEQUENCE_TAG);

					// setting plugin properties
					plugin.setPluginId(md5Generator.getMd5(pluginDirectory.getName()));
					plugin.setExecutionPath(execPathNode.getText());
					plugin.setName(nameNode.getText());
					plugin.setPluginSeq(Integer.parseInt(sequenceNode.getText()));

					File pluginTestsXmlFile = new File(pluginDirectory.getAbsolutePath() + "/" + Constants.TEST_XML);
					Document pluginTestsXml = reader.read(pluginTestsXmlFile);
					Element rootNode = pluginTestsXml.getRootElement();

					Collection<PluginTest> tests = new ArrayList<PluginTest>();

					// -----------------setting tests start
					for (Iterator i = rootNode.elementIterator(Constants.TEST_XML_SINGLE_TEST_TAG); i.hasNext();) {

						Element testRootNode = (Element) i.next();

						PluginTest pluginTest = new PluginTest();
						String testName;						
						String argument = "";
						String successCondition;
						String successConnditionType;
						String severity;
						String reportXmlName;
						Boolean requireParameterizedUrl;
						Integer testSeq;
						
						testName = testRootNode.element(Constants.TEST_XML_SINGLE_TEST_NAME_TAG)
								.getText();
						System.out.println("---------------------" + testName);
						testSeq = Integer.parseInt(testRootNode
								.element(Constants.TEST_XML_SINGLE_TEST_SEQUENCE_TAG).getText());
						
						severity = testRootNode.element(Constants.TEST_XML_SINGLE_TEST_SEVERITY_TAG)
								.getText();
						
						requireParameterizedUrl = testRootNode.element(Constants.TEST_XML_SINGLE_TEST_NEED_PARAMETERIZED_URL_TAG)
								.getText().equals("Y") ? true : false ;
						
						reportXmlName = testRootNode.element(Constants.TEST_XML_SINGLE_TEST_REPORT_XML_NAME_TAG)
								.getText();

						// -----Construct argument start---------
						for (Iterator j = testRootNode.element(Constants.TEST_XML_SINGLE_TEST_ARGUMENTS_ROOT_TAG).elementIterator(Constants.TEST_XML_SINGLE_TEST_ARGUMENT_TAG); j
								.hasNext();) {
							Element argumentNode = (Element) j.next();
							
							System.out.println("-----------| " + argumentNode.attributeValue(Constants.TEST_XML_SINGLE_TEST_ARGUMENT_HAS_DOUBLE_HIPHEN_ATTR));
							// appending hyphen(s)
							if (argumentNode.attributeValue(
									Constants.TEST_XML_SINGLE_TEST_ARGUMENT_HAS_DOUBLE_HIPHEN_ATTR).equals("Y")) {
								argument += "--";
							} else {
								argument += "-";
							}

							// appending argument name
							argument += argumentNode.attributeValue(Constants.TEST_XML_SINGLE_TEST_ARGUMENT_NAME_ATTR);

							// appending value
							if (!argumentNode
									.attributeValue(Constants.TEST_XML_SINGLE_TEST_ARGUMENT_IS_DYNAMIC_ATTR).equals("Y")) {
								// Not dynamic
								if (argumentNode.attributeValue(
										Constants.TEST_XML_SINGLE_TEST_ARGUMENT_HAS_VALUE_ATTR).equals("Y")) {
									// has value
									argument += argumentNode.attributeValue(
											Constants.TEST_XML_SINGLE_TEST_REPORT_XML_SEPARATER_ATTR) + argumentNode
											.attributeValue(Constants.TEST_XML_SINGLE_TEST_ARGUMENT_VALUE_ATTR);
								}
							} else {
								// dynamic
								// has value(implicit)
								argument += argumentNode.attributeValue(
										Constants.TEST_XML_SINGLE_TEST_REPORT_XML_SEPARATER_ATTR) + "@" + argumentNode
										.attributeValue(Constants.TEST_XML_SINGLE_TEST_ARGUMENT_VALUE_ATTR);
							}

							argument += " ";
						}
						// -----Construct argument ends---------

						// getting success condition
						Node conditionNode = testRootNode
								.selectSingleNode("//" + Constants.TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_ROOT_TAG + "/"
										+ Constants.TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_TAG);

						if (conditionNode == null) {
							System.out.println("-----------------------conditionNode is null");
						} else {
							System.out.println("-----------------------conditionNode is NOT null");
						}
						successConnditionType = conditionNode
								.valueOf("@" + Constants.TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_TYPE_ATTR);
						successCondition = conditionNode
								.valueOf("@" + Constants.TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_VALUE_ATTR);

						// setting attributes to plugin
						pluginTest.setTestId(md5Generator.getMd5(testName));
						pluginTest.setName(testName);
						pluginTest.setTestSeq(testSeq);
						pluginTest.setArguments(argument);
						pluginTest.setSuccessCondition(successCondition);
						pluginTest.setSuccessConditionType(successConnditionType);
						pluginTest.setIsEnabled(false);
						pluginTest.setState("Functional");
						pluginTest.setPlugin(plugin);
						pluginTest.setSeverity(severity);
						pluginTest.setRequireParameterizedUrl(requireParameterizedUrl);
						pluginTest.setReportPath(pluginDirectory.getAbsolutePath() + "/" + Constants.REPORTS_DIRECTORY + "/" + reportXmlName) ;
						
						//get the report fields.
						File reportXmlFile = new File(pluginTest.getReportPath());
						Document reportXml = reader.read(reportXmlFile);
						Node descriptionNode = reportXml.selectSingleNode("//description");
						Node impactNode = reportXml.selectSingleNode("//impact");
						Node solutionNode = reportXml.selectSingleNode("//solution");
						Node referencesNode = reportXml.selectSingleNode("//references");
						
						pluginTest.setDescription(descriptionNode.getText());
						pluginTest.setImpact(impactNode.getText());
						pluginTest.setSolution(solutionNode.getText());
						pluginTest.setReference(referencesNode.asXML().replace("<references>", "").replace("</references>", ""));
						
						
						// adding test to test collection
						tests.add(pluginTest);
					}
					// -----------------setting tests ends

					plugin.setListOfTests(tests);
					for(PluginTest test: plugin.getListOfTests()){
						System.out.println("------------" + test.getTestId());
					}
					// insert plugin to data base
					pluginDao.addPlugin(plugin);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void disablePluginTest(String testId) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<PluginTest> getAllPluginTests() {
		return pluginDao.getAllPluginTest();
	}

	@Override
	public List<PluginTest> getTestsByPlugin(String pluginId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addPlugin(Plugin plugin) {
		// TODO Auto-generated method stub

	}

}
