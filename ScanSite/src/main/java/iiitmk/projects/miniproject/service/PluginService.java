package iiitmk.projects.miniproject.service;

import java.util.List;

import iiitmk.projects.miniproject.beans.Plugin;
import iiitmk.projects.miniproject.beans.PluginTest;

public interface PluginService {
	Boolean loadPlugins();
	void disablePluginTest(String testId);
	List<PluginTest> getAllPluginTests();
	List<PluginTest> getTestsByPlugin(String pluginId);
	void addPlugin(Plugin plugin);
}
