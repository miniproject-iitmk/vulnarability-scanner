package iiitmk.projects.miniproject.commons;

import org.springframework.stereotype.Component;

@Component
public class Constants {
	public static final String PLUGIN_DEF_XML = "plugin_definition.xml";
	public static final String PLUGIN_DEF_XML_ROOT_TAG = "plugin";
	public static final String PLUGIN_DEF_XML_NAME_TAG = "name";
	public static final String PLUGIN_DEF_XML_EXEC_PATH_TAG = "execution_path";
	public static final String PLUGIN_DEF_XML_SEQUENCE_TAG = "sequence";
	public static final String TEST_XML = "tests.xml";
	public static final String TEST_XML_ROOT_TAG = "tests";
	public static final String TEST_XML_SINGLE_TEST_TAG = "test";
	public static final String TEST_XML_SINGLE_TEST_NAME_TAG = "name";
	public static final String TEST_XML_SINGLE_TEST_SEQUENCE_TAG = "sequence";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENTS_ROOT_TAG = "arguments";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENT_TAG = "argument";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENT_NAME_ATTR = "name";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENT_HAS_VALUE_ATTR = "hasValue";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENT_VALUE_ATTR = "value";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENT_HAS_DOUBLE_HIPHEN_ATTR = "hasDoubleHiphen";
	public static final String TEST_XML_SINGLE_TEST_ARGUMENT_IS_DYNAMIC_ATTR = "isDynamic";
	public static final String TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_ROOT_TAG = "success-condition";
	public static final String TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_TAG = "condition";
	public static final String TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_TYPE_ATTR = "type";
	public static final String TEST_XML_SINGLE_TEST_SUCCESS_CONDITION_VALUE_ATTR = "value";
	public static final String TEST_XML_SINGLE_TEST_SEVERITY_TAG = "severity";
	public static final String TEST_XML_SINGLE_TEST_NEED_PARAMETERIZED_URL_TAG = "require-parameterized-url";
	public static final String TEST_XML_SINGLE_TEST_REPORT_XML_NAME_TAG = "report-xml-name";
	public static final String TEST_XML_SINGLE_TEST_REPORT_XML_SEPARATER_ATTR = "separator";
	public static final String SCAN_TYPE_FULL = "full";
	public static final String REPORTS_DIRECTORY = "reports";
	public static final String SCAN_STATUS_INITIAL = "Intializing";
	public static final String SCAN_STATUS_CRAWLING = "Crawling";
	public static final String DYNAMIC_ARG_URL = "@url";
	public static final String DYNAMIC_ARG_UA = "@user-agent";
	public static final String DYNAMIC_ARG_COOKIE = "@cookie";
	public static final String SEVERITY_HIGH = "High";
	public static final String SEVERITY_MEDIUM = "Medium";
	public static final String SEVERITY_LOW = "Low";



}
