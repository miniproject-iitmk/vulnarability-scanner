package iiitmk.projects.miniproject.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
/*@IdClass(ScanRecordDetailedPK.class)*/
@Table(name = "scan_records_detailed")
public class ScanRecordDetailed {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="mapping_id")
	private Integer mappingId;

	@ManyToOne
    @JoinColumn(name = "scan_record_id")
	private ScanRecordAbstract scanRecord;


	@ManyToOne
    @JoinColumn(name = "test_id")
	private PluginTest test;
	
	@ManyToOne
    @JoinColumn(name = "url_mapping_id")
	private ScanUrlCollection url;

	@Column(name = "severity")
	private String severity;

	public ScanRecordAbstract getScanRecord() {
		return scanRecord;
	}

	public void setScanRecord(ScanRecordAbstract scanRecord) {
		this.scanRecord = scanRecord;
	}

	public PluginTest getTest() {
		return test;
	}

	public void setTest(PluginTest test) {
		this.test = test;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public ScanUrlCollection getUrl() {
		return url;
	}

	public void setUrl(ScanUrlCollection url) {
		this.url = url;
	}

	public Integer getMappingId() {
		return mappingId;
	}

	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}
	
}
