package iiitmk.projects.miniproject.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UserRoleMappingPK.class) 
@Table(name="user_role_mapping")
public class UserRoleMapping{
	
	@Id	
	private User user;
	
	@Id	
	private Role role;
	
	@Column(name="is_default")
	private Boolean isDefault;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
}
