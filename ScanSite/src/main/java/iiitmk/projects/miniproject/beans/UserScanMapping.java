package iiitmk.projects.miniproject.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UserScanMappingPK.class)
@Table(name="user_scan_mappings")
public class UserScanMapping {
	
	@Id
	private User user;
	
	@Id
	private ScanSettings scan;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ScanSettings getScan() {
		return scan;
	}

	public void setScan(ScanSettings scan) {
		this.scan = scan;
	}
	
}
