package iiitmk.projects.miniproject.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="scan_records_abstract")
public class ScanRecordAbstract {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="scan_record_id")
	private int scanRecordId;
	
	@ManyToOne
	@JoinColumn(name="scan_id")
	private ScanSettings scan;
	
	@Column(name="count_high")
	private int countHigh;
	
	@Column(name="count_medium")
	private int countMedium;
	
	@Column(name="count_low")
	private int countLow;
	
	@Column(name="scanned_on")
	private Date scannedOn;
	
	@OneToMany(mappedBy = "scanRecord")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<ScanRecordDetailed> scanRecords = new ArrayList<ScanRecordDetailed>();

	@ManyToMany
	@JoinTable(name = "scan_records_detailed", joinColumns = { @JoinColumn(name = "scan_record_id") }, inverseJoinColumns = {
			@JoinColumn(name = "test_id") })
	private Collection<PluginTest> tests = new ArrayList<PluginTest>();	

	public int getScanRecordId() {
		return scanRecordId;
	}
	
	public Collection<ScanRecordDetailed> getScanRecords() {
		return scanRecords;
	}

	public void setScanRecords(Collection<ScanRecordDetailed> scanRecords) {
		this.scanRecords = scanRecords;
	}

	public void setScanRecordId(int scanRecordId) {
		this.scanRecordId = scanRecordId;
	}

	

	public ScanSettings getScan() {
		return scan;
	}

	public void setScan(ScanSettings scan) {
		this.scan = scan;
	}

	public int getCountHigh() {
		return countHigh;
	}

	public void setCountHigh(int countHigh) {
		this.countHigh = countHigh;
	}

	public int getCountMedium() {
		return countMedium;
	}

	public void setCountMedium(int countMedium) {
		this.countMedium = countMedium;
	}

	public int getCountLow() {
		return countLow;
	}

	public void setCountLow(int countLow) {
		this.countLow = countLow;
	}

	public Date getScannedOn() {
		return scannedOn;
	}

	public void setScannedOn(Date scannedOn) {
		this.scannedOn = scannedOn;
	}

	public Collection<PluginTest> getTests() {
		return tests;
	}

	public void setTests(Collection<PluginTest> tests) {
		this.tests = tests;
	}			
}
