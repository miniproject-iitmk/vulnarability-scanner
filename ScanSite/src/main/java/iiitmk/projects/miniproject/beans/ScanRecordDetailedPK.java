/*package iiitmk.projects.miniproject.beans;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class ScanRecordDetailedPK implements Serializable  {
	private static final long serialVersionUID = 1L;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "scan_record_id", referencedColumnName = "scan_record_id")
	private ScanRecordAbstract scanRecord;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "test_id", referencedColumnName = "test_id")
	private PluginTest test;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mapping_id", referencedColumnName = "mapping_id")
	private ScanUrlCollection url;

	public ScanRecordAbstract getScanRecord() {
		return scanRecord;
	}

	public void setScanRecord(ScanRecordAbstract scanRecord) {
		this.scanRecord = scanRecord;
	}

	public PluginTest getTest() {
		return test;
	}

	public void setTest(PluginTest test) {
		this.test = test;
	}

	public ScanUrlCollection getUrl() {
		return url;
	}

	public void setUrl(ScanUrlCollection url) {
		this.url = url;
	}
	
}
*/