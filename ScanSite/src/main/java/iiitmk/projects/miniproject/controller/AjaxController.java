package iiitmk.projects.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.service.ScanService;

@RestController
public class AjaxController {
	
	@Autowired
	ScanService scanService;
	
	@ResponseBody 
	@RequestMapping(value = "/insertNewScan", method = RequestMethod.POST)
	public String insertNewScan(@RequestBody ScanSettings scanSettings) {
		
		return scanService.insertNewScan(scanSettings) + "";
	}
	
	@ResponseBody 
	@RequestMapping(value = "/crawlUrls", method = RequestMethod.POST)
	public String getListOfTest(@RequestBody ScanSettings scanSettings ) {
		return scanService.getCrawledUrls(scanSettings.getPrimaryUrl()).toString();
	}
}
