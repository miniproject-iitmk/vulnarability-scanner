package iiitmk.projects.miniproject.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import iiitmk.projects.miniproject.beans.ScanSettings;
import iiitmk.projects.miniproject.service.PluginService;
import iiitmk.projects.miniproject.service.RecordService;
import iiitmk.projects.miniproject.service.ScanService;
import iiitmk.projects.miniproject.service.UserService;
import iiitmk.projects.miniproject.utils.ScanScheduler;

@Controller
public class MainController {

	@Autowired
	UserService userService;
	
	@Autowired
	ScanService scanService;
	
	@Autowired
	PluginService pluginService;
	
	@Autowired
	RecordService recordService;
	
	@Autowired
	private ApplicationContext ctx;
	
	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
	public String homePage(ModelMap model) {
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));		
		return "homePage";
	}

	/*@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPage(ModelMap model) {
		model.addAttribute("user", getPrincipal());
		return "admin";
	}

	@RequestMapping(value = "/db", method = RequestMethod.GET)
	public String dbaPage(ModelMap model) {
		model.addAttribute("user", getPrincipal());
		return "dba";
	}*/

	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("user", getPrincipal());
		return "accessDenied";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		return "loginPage";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	@RequestMapping(value = { "/manageUser" }, method = RequestMethod.GET)
	public String userPage(ModelMap model) {
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));
		model.addAttribute("userList", userService.getAllUsers());
		return "users";
	}

	@RequestMapping(value = { "/manageAccounts" }, method = RequestMethod.GET)
	public String accountPage(ModelMap model) {
		model.addAttribute("greeting", "Hi, Welcome to mysite");
		return "account";
	}

	@RequestMapping(value = { "/scanReport/{reportId}" }, method = RequestMethod.GET)
	public String Report(@PathVariable("reportId") int reportId, ModelMap model) {
		model.addAttribute("listOfRecords", recordService.getAbstractRecordById(reportId).getScanRecords());
		model.addAttribute("scanId", recordService.getAbstractRecordById(reportId).getScan().getScanId());
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));
		return "report";
	}

	
	@RequestMapping(value = { "/previousReports/{scanId}" }, method = RequestMethod.GET)
	public String previousReports(@PathVariable("scanId") int scanId, ModelMap model) {
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));
		model.addAttribute("abstractRecords", scanService.getAbstractRecords(scanId));
		model.addAttribute("scanId", scanId);	
		return "previousReport";
	}
	
	@RequestMapping(value = {  "/activeScan" }, method = RequestMethod.GET)
	public String Status(ModelMap model) {
		ScanSettings activeScan = scanService.getActiveScansByUsername(getPrincipal());
		Integer activeScanId = null;
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		if(activeScan != null){
			activeScanId = activeScan.getScanId();
			model.addAttribute("scanRecord", recordService.getAbstractRecordById(activeScan.getActiveScanRecordId()));
		}
		model.addAttribute("activeScan", activeScan);
		model.addAttribute("scanId", activeScanId);		
		return "status";

	}

	@RequestMapping(value = { "/addUser" }, method = RequestMethod.POST)
	public String addUser(@RequestParam("user_name") String userName, @RequestParam("password") String password,  @RequestParam("selection") String role ) {

		userService.addUser(userName, password, role);
		
		return "redirect:/manageUser";
	}

	
	@RequestMapping(value = {  "/detailedReport/{reportId}" }, method = RequestMethod.GET)
	public String activeScan(@PathVariable("reportId") int reportId, ModelMap model) {
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));
		model.addAttribute("detailedReport", recordService.getDetailedRecordById(reportId));	
		model.addAttribute("scanId", recordService.getDetailedRecordById(reportId).getScanRecord().getScan().getScanId());
		return "reportExpanded";
	}
	
	@RequestMapping(value = {  "/managePlugins" }, method = RequestMethod.GET)
	public String managePlugin(ModelMap model) {
		model.addAttribute("greeting", "Hi, Welcome to mysite");
		return "plugins";
	}
	
	@RequestMapping(value = {  "/scanSettings/{scanId}" }, method = RequestMethod.GET)
	public String scanSettings(@PathVariable("scanId") int scanId, ModelMap model) {
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));
		model.addAttribute("scanId", scanId);
		model.addAttribute("scanDetails", scanService.getScanById(scanId));
		return "previous_scan";
	}
	
	@RequestMapping(value = {  "/newScan" }, method = RequestMethod.GET)
	public String newScan(ModelMap model) {
		model.addAttribute("inactiveScans", scanService.getInactiveScansByUsername(getPrincipal()));
		model.addAttribute("activeScan", scanService.getActiveScansByUsername(getPrincipal()));
		model.addAttribute("listOfTests", pluginService.getAllPluginTests());
		return "newScan";
	}
	
	@RequestMapping(value = {  "/startScan/{scanId}" }, method = RequestMethod.GET)
	public String startScan(@PathVariable("scanId") int scanId, ModelMap model) {
		ScanScheduler scheduler = new ScanScheduler();
		scheduler.setScanSettings(scanService.getScanById(scanId));
		scheduler.setRecordService(recordService);
		scheduler.setScanService(scanService);
		scheduler.executeTaskT();
		return "redirect:/activeScan";
	}
	
	
	
	private String getPrincipal(){
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

}