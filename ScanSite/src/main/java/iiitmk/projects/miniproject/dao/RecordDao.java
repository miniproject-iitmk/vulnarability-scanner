package iiitmk.projects.miniproject.dao;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;

public interface RecordDao {

	int insertRecord(ScanRecordAbstract scanRecord);

	void updateRecord(ScanRecordAbstract scanRecord);

	ScanRecordAbstract getAbstractRecordById(Integer id);
	
}
