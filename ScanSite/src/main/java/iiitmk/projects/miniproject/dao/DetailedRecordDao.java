package iiitmk.projects.miniproject.dao;

import iiitmk.projects.miniproject.beans.ScanRecordDetailed;

public interface DetailedRecordDao {
	ScanRecordDetailed getDetailedRecordById(Integer key);
	Integer insertDetailedScanRecord(ScanRecordDetailed scanRecord);
}
