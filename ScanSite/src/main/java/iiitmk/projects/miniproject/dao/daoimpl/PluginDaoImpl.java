package iiitmk.projects.miniproject.dao.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.beans.Plugin;
import iiitmk.projects.miniproject.beans.PluginTest;
import iiitmk.projects.miniproject.dao.AbstractDao;
import iiitmk.projects.miniproject.dao.PluginDao;

@Repository("pluginDao")
public class PluginDaoImpl extends AbstractDao<Integer, Plugin> implements PluginDao  {

	@Override
	public void addPlugin(Plugin plugin) {
		merge(plugin);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PluginTest> getAllPluginTest() {
		Criteria crit = createEntityCriteria();
		List<Plugin> plugins = crit.list();
		List<PluginTest> tests = new ArrayList<PluginTest>();
		
		for(Plugin plugin : plugins){
			for(PluginTest test: plugin.getListOfTests()){
				tests.add(test);
			}
		}
		
		return tests;
	}

}
