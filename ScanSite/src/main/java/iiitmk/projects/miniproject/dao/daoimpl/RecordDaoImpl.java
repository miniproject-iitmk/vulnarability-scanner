package iiitmk.projects.miniproject.dao.daoimpl;

import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.dao.AbstractDao;
import iiitmk.projects.miniproject.dao.RecordDao;

@Repository("recordDao")
public class RecordDaoImpl extends AbstractDao<Integer, ScanRecordAbstract> implements RecordDao  {

	@Override
	public int insertRecord(ScanRecordAbstract scanRecord) {
		// TODO Auto-generated method stub
		return save(scanRecord);
	}

	@Override
	public void updateRecord(ScanRecordAbstract scanRecord) {
		merge(scanRecord);
		
	}

	@Override
	public ScanRecordAbstract getAbstractRecordById(Integer id) {
		// TODO Auto-generated method stub
		return getByKey(id);
	}

}
