package iiitmk.projects.miniproject.dao;

import java.util.Collection;

import iiitmk.projects.miniproject.beans.ScanRecordAbstract;
import iiitmk.projects.miniproject.beans.ScanSettings;

public interface ScanDao {
	ScanSettings findByScanId(int Scanid);

	Collection<ScanSettings> getAllScanId();

	void updateScanStatus(int scanId, String scanStatus, int percentageComplete, Boolean isActive, Integer recordPk);

	Integer insertNewScan(ScanSettings scanSettings);

	Collection<ScanSettings> getInactiveScansByUsername(String userName);

	ScanSettings getActiveScansByUsername(String userName);

	Collection<ScanRecordAbstract> getAbstractRecords(Integer scanId);
}
