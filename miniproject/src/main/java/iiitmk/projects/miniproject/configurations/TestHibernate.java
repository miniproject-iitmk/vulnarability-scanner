package iiitmk.projects.miniproject.configurations;

import java.util.Properties;

import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@PropertySource(value = { "classpath:application.properties" })
public class TestHibernate {
	/*public static void main(String args[]) {
		MockEnvironment environment = new MockEnvironment();
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		environment.setProperty("jdbc.driverClassName", "org.postgresql.Driver");
		environment.setProperty("jdbc.url", "jdbc:postgresql://localhost:5432/mini_project");
		environment.setProperty("jdbc.username", "postgres");
		environment.setProperty("jdbc.password", "postgres");
		environment.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		environment.setProperty("hibernate.show_sql", "true");
		environment.setProperty("hibernate.format_sql", "true");

		dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
		dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
		dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
		System.out.println("Done!");

		sessionFactory.setDataSource(dataSource);
		sessionFactory.setPackagesToScan(new String[] { "iiitmk.projects.miniproject.core.beans" });
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		sessionFactory.setHibernateProperties(properties);
		sessionFactory.getObject();

	}*/
}
