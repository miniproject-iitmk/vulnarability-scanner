package iiitmk.projects.miniproject.core.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import iiitmk.projects.miniproject.core.beans.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	public User findById(int id) {
		return getByKey(id);
	}

	public User findByName(String userName) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("userId", userName));
		return (User) crit.uniqueResult();
	}

	
}
