package iiitmk.projects.miniproject.core.beans;

public class Plugin {
	private int pluginId;
	private int pluginSeq;
	private String name;

	public int getPluginId() {
		return pluginId;
	}

	public void setPluginId(int pluginId) {
		this.pluginId = pluginId;
	}

	public int getPluginSeq() {
		return pluginSeq;
	}

	public void setPluginSeq(int pluginSeq) {
		this.pluginSeq = pluginSeq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
