package iiitmk.projects.miniproject.core.dao;

import iiitmk.projects.miniproject.core.beans.User;

public interface UserDao {

	User findById(int id);	
	User findByName(String userName);
	
}
