package iiitmk.projects.miniproject.core.beans;

public class ScanTestMapping {
	private int scanId;
	private int testId;

	public int getScanId() {
		return scanId;
	}

	public void setScanId(int scanId) {
		this.scanId = scanId;
	}

	public int getTestId() {
		return testId;
	}

	public void setTestId(int testId) {
		this.testId = testId;
	}

}
