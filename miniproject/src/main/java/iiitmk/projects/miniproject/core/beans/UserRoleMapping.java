package iiitmk.projects.miniproject.core.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UserRoleMappingPK.class) 
@Table(name="user_role_mapping")
public class UserRoleMapping {
	
	@Id
	@Column(name="user_id")
	private int userId;
	
	@Id
	@Column(name="role_id")
	private int roleId;
	
	@Column(name="is_default")
	private boolean isDefault;		

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

}
