package iiitmk.projects.miniproject.core.dao;

import iiitmk.projects.miniproject.core.beans.Role;

public interface RoleDao {
 	Role findById(int roleId);
}
