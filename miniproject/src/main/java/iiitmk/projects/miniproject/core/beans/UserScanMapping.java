package iiitmk.projects.miniproject.core.beans;

public class UserScanMapping {
	private int userId;
	private int scanId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getScanId() {
		return scanId;
	}

	public void setScanId(int scanId) {
		this.scanId = scanId;
	}

}
