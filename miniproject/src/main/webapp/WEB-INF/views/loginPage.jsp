<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="stylesheet" href="resources/css/foundation.css" />
<!-- the app.css file must be used to write custom css. Should not make any modifications in any other css files  -->
<link rel="stylesheet" href="resources/css/app.css" />
<link href="resources/css/font-awesome.min.css" rel="stylesheet"
	type='text/css'>
<link href='resources/css/foundation-icons.css' rel='stylesheet'
	type='text/css'>
</head>
<body>
	<div class="row">
		<div class="medium-6 medium-centered large-3 large-centered columns" id="login">
			<form action="login" method="post">
				<div class="column log-in-form">
					<h4 class="text-center">Log in to Scan Site</h4>
					<label>Username <input type="text"
						placeholder="Username" name="username">
					</label> <label>Password <input type="password" placeholder="Password" name="password">
					</label> <input id="remember-me" type="checkbox"><label
						for="remember-me">Remember me</label>
						                            <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
					<p>
						<a type="submit" class="button expanded">Log In</a>
					</p>
				</div>
			</form>
		</div>
	</div>
</body>
</html>